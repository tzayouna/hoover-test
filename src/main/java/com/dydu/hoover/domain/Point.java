package com.dydu.hoover.domain;

/**
 * Point class
 * This POJO aims to have a more object oriented
 * space representation
 */
public class Point {

    private int x;
    private int y;
    private boolean wall;
    private int hoverings = 0;

    public Point(int x, int y, String character) {
        this.x = x;
        this.y = y;
        this.wall = character.toUpperCase().equals("M");
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        return y == point.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    /**
     * Convenience method to check if 2 points are next to each other
     *
     * @param currentPoint
     * @return
     */
    public boolean isNextTo(Point currentPoint) {
        int absX = Math.abs(x - currentPoint.getX());
        int absY = Math.abs(y - currentPoint.getY());
        return (absX == 0 && absY == 1) || (absY == 0 && absX == 1);
    }

    /**
     * Increments the hover counter
     */
    public void setHovered() {
        ++this.hoverings;
    }

    /*************************
     GETTERS
     *************************/
    public boolean isHovered() {
        return hoverings > 0;
    }

    public int getHoverings() {
        return hoverings;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isWall() {
        return wall;
    }
}
