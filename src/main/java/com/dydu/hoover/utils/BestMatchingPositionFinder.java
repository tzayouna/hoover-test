package com.dydu.hoover.utils;

import com.dydu.hoover.domain.Point;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Finder to get the best matching points
 */
public class BestMatchingPositionFinder {

    private List<Point> points;
    private Point lastKnownPoint;

    /**
     * Returns the best matching point to hover
     * @return Point
     */
    public Point getBestMatchingPoint() {
        points.remove(lastKnownPoint);
        if (points.size() == 0) {
            return lastKnownPoint;
        }
        return Stream.of(
            unhovered(),
            leastHovered()
        )
                .filter(Optional::isPresent)
                .findFirst()
                .map(Optional::get)
                .get();
    }

    /**
     * Gets the least hovered point in order to avoid getting stuck
     * @return Optional<Point>
     */
    private Optional<Point> leastHovered() {
        return points
                .stream()
                .sorted(Comparator.comparingInt(Point::getHoverings))
                .findFirst();
    }

    /**
     * Gets an Optional of a unhovered point
     * @return Optional<Point>
     */
    private Optional<Point> unhovered() {
        return points
                .stream()
                .filter(p -> !p.isHovered())
                .findFirst();
    }

    /**
     * Assign a list of point candidated in a fluent manner
     * @param nearestPoints
     * @return BestMatchingPositionFinder
     */
    public BestMatchingPositionFinder withNearestPoints(List<Point> nearestPoints) {
        points = nearestPoints;
        return this;
    }

    /**
     * Simple setter to keep track of the last known point
     * @param point
     */
    public void applyLastKnownPoint(Point point) {
        this.lastKnownPoint = point;
    }
}
