package com.dydu.hoover;

import com.dydu.hoover.domain.Point;
import com.dydu.hoover.utils.BestMatchingPositionFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * This class represents the Hoover
 * Get your instance through the Runner
 */
public class Hoover {

    private static final Logger LOG = LoggerFactory.getLogger(Hoover.class);
    private Set<Point> availablePoints;
    private List<Consumer<Point>> consumers;
    private Point currentPoint;
    private BestMatchingPositionFinder finder = new BestMatchingPositionFinder();
    private int hoverLimit = 15;

    /**
     * Constructor
     * Provider the room's matrix
     *
     * @param room
     * @param consumer
     */
    private Hoover(String[][] room, Consumer<Point> consumer) {
        this.availablePoints = new HashSet<>();
        this.consumers = new ArrayList<>();
        this.consumers.add(consumer);
        for (int y = 0; y < room.length; y++) {
            for (int x = 0; x < room[y].length; x++) {
                availablePoints.add(new Point(x + 1, y + 1, room[y][x]));
            }
        }
    }

    /**
     * Checks if the room is finished
     * @return boolean
     */
    public boolean hasFinishedRoom() {
        return availablePoints
                .stream()
                .filter(p -> !p.isWall() && !p.isHovered())
                .count() == 0;
    }

    /**
     * Checks if any point in the matrix has reached the provided
     * hover limit
     * @return boolean
     */
    public boolean hasReachedHoverLimit() {
        return availablePoints
                .stream()
                .filter(p -> p.getHoverings() >= hoverLimit)
                .count() > 0;
    }

    /**
     * Gets the highest hover rate reached for this run
     * @return int
     */
    private int getHighestHoverRate() {
        return availablePoints
                .stream()
                .filter(p -> !p.isWall())
                .map(Point::getHoverings)
                .max(Integer::compareTo)
                .get();
    }

    /**
     * Sets the point and applies it to finder too
     * before propating the new point to all the hoover's
     * consumers
     * @param point
     */
    private void setPointAt(Point point) {
        point.setHovered();
        this.currentPoint = point;
        finder.applyLastKnownPoint(point);
        consumers.forEach(c -> c.accept(point));
        if (point.getHoverings() >= hoverLimit) {
            LOG.warn("Hover limit of {} has been reached, the hoover will stop", hoverLimit);
        }
    }

    /**
     * Start the hoover
     * @param point
     */
    public void start(Point point) {
        setPointAt(point);
        while (!hasFinishedRoom() && !hasReachedHoverLimit()) {
            setPointAt(finder.withNearestPoints(getNearestPoints())
                    .getBestMatchingPoint());
        }
        printDebugReport();
    }

    /**
     * Prints the debug report of this run
     */
    private void printDebugReport() {
        LOG.debug("Finished hovering with highest hover rate {}", getHighestHoverRate());
        if (!hasFinishedRoom()) {
            LOG.debug("Could not cleanup entire room, the following fields could not be hovered");
            getUnhoveredPoints()
                    .forEach(p -> LOG.debug("Point {} could not be hovered", p));
        }
    }

    /**
     * Gets all the points that are close to the current point
     * @return List<Point>
     */
    private List<Point> getNearestPoints() {
        return availablePoints
                .stream()
                .filter(p -> !p.isWall())
                .filter(p -> p.isNextTo(currentPoint))
                .collect(Collectors.toList());
    }

    /**
     * Gets all unhovered points when the hoover gets stuck
     * @return Set<Point>
     */
    public Set<Point> getUnhoveredPoints() {
        return availablePoints
                .stream()
                .filter(p -> !p.isWall() && !p.isHovered())
                .collect(Collectors.toSet());
    }

    /**
     * Sets a hoverLimit to avoid endless hovering
     * @param hoverLimit
     */
    public void setHoverLimit(int hoverLimit) {
        this.hoverLimit = hoverLimit;
    }

    /**
     * Runner class
     * More encapsulated way to build a Hoover
     */
    public static class Runner {
        private Hoover hoover;

        /**
         * Given a room matrix and a consumer
         * @param room
         * @param consumer
         */
        public Runner(String[][] room, Consumer<Point> consumer) {
            this.hoover = new Hoover(room, consumer);
        }

        public Runner withFieldHoverLimit(int hoverLimit) {
            hoover.setHoverLimit(hoverLimit);
            return this;
        }

        /**
         * Runs the Hoover from the given x and y coordinates
         * @param x
         * @param y
         * @return Hoover
         */
        public Hoover run(int x, int y) {
            hoover.start(new Point(x, y, ""));
            return hoover;
        }
    }
}
