package com.dydu.hoover.utils;

import com.dydu.hoover.domain.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test BestMatchingPositionFinder
 */
public class BestMatchingPositionFinderTest {

    private BestMatchingPositionFinder finder;

    @Before
    public void setup() {
        finder = new BestMatchingPositionFinder();
    }

    @Test
    public void testUnhovered() {
        List<Point> points = new ArrayList<>();
        finder.applyLastKnownPoint(new Point(3, 3, ""));

        Point hovered1 = new Point(3, 4, "");
        hovered1.setHovered();
        hovered1.setHovered();
        hovered1.setHovered();

        Point hovered2 = new Point(3, 2, "");
        hovered2.setHovered();
        hovered2.setHovered();

        Point hovered3 = new Point(2, 3, "");
        hovered3.setHovered();
        hovered3.setHovered();

        Point unhovered = new Point(4, 3, "");

        points.add(hovered1);
        points.add(hovered2);
        points.add(hovered3);
        points.add(unhovered);

        assertEquals(finder.withNearestPoints(points).getBestMatchingPoint(), unhovered);

        unhovered.setHovered();

        assertEquals(finder.withNearestPoints(points).getBestMatchingPoint(), unhovered);
    }
}
