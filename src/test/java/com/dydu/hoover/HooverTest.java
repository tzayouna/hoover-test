package com.dydu.hoover;

import com.dydu.hoover.utils.MatrixFileReader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HooverTest {

    private String[][] matrix;
    private String[][] matrix2;
    private String[][] matrixComplex;
    private String[][] matrixComplex2;
    private String[][] unreachableArea;

    @Before
    public void setupMatrix() {
        MatrixFileReader matrixFileReader = new MatrixFileReader();
        matrix = matrixFileReader.readFile("/maze1.txt");
        matrix2 = matrixFileReader.readFile("/hoover1.txt");
        matrixComplex = matrixFileReader.readFile("/complex.txt");
        matrixComplex2 = matrixFileReader.readFile("/big.txt");
        unreachableArea = matrixFileReader.readFile("/big_unreachable.txt");
    }

    @Test
    public void testHoover() {
        Hoover hoover = new Hoover.Runner(matrixComplex, System.out::println).run(3, 3);
        assertTrue(hoover.hasFinishedRoom());

        Hoover hoover2 = new Hoover.Runner(matrix, System.out::println).run(3, 3);
        assertTrue(hoover2.hasFinishedRoom());

        Hoover hoover3 = new Hoover.Runner(matrix2, System.out::println).run(3, 3);
        assertTrue(hoover3.hasFinishedRoom());

        Hoover hoover4 = new Hoover.Runner(matrixComplex2, System.out::println).run(2, 3);
        assertTrue(hoover4.hasFinishedRoom());

        Hoover hoover5 = new Hoover.Runner(unreachableArea, System.out::println).run(2, 3);
        assertFalse(hoover5.hasFinishedRoom());
        assertTrue(hoover5.hasReachedHoverLimit());
        assertEquals(6, hoover5.getUnhoveredPoints().size());
    }
}
